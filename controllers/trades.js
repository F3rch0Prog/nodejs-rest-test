/**
 * {
 *    "id": 1,
 *    "type": "buy",                        "buy" or "sell"
 *    "user": {
 *       "id": 1,
 *       "name": "David"
 *    },
 *    "symbol": "AC",
 *    "shares": 28,                         between 20 and 30 inclusive
 *    "price": 162.17,                      between 130.42 and 195.65 inclusive. Two decimal values
 *    "timestamp": "2014-06-14 13:13:13"    UTC-4 timezone
 * }
 */
const trades = [];

const getTrades = () => {
  return trades;
};

const clearTrades = () => {
  trades.splice(0, trades.length);
};

const hasTrade = (tradeId) => {
  tradeId = parseInt(tradeId);

  return -1 !== trades.map((trade) => trade.id).indexOf(tradeId);
};

const isValidTrade = (trade) => {
  const validKeys = [
      "id",
      "type",
      "user",
      "symbol",
      "shares",
      "price",
      "timestamp",
    ],
    suppliedKeys = Object.keys(trade);
  let valid = true;

  for (const key of validKeys) {
    if (-1 === suppliedKeys.indexOf(key)) {
      valid = false;
      break;
    }
  }

  valid = valid && -1 !== ["buy", "sell"].indexOf(trade.type);
  valid = valid && 20 <= trade.shares && trade.shares <= 30;
  valid = valid && 130.42 <= trade.price && trade.price <= 195.65;

  return valid;
};

const addTrade = (trade) => {
  trade.price = parseFloat(trade.price).toFixed(2);

  trades.push(trade);
};

module.exports.getTrades = getTrades;
module.exports.clearTrades = clearTrades;
module.exports.hasTrade = hasTrade;
module.exports.isValidTrade = isValidTrade;
module.exports.addTrade = addTrade;
