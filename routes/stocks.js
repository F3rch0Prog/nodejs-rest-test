const express = require("express");
const router = express.Router();
const tradesController = require("../controllers/trades");
const moment = require("moment");

// Get stock trades ?type={tradeType}&start={startDate}&end={endDate}
// Stock symbol does not exist 404
// 200 otherwise
router.get("/:stockSymbol/trades", (req, res, next) => {
  const stockSymbol = req.params.stockSymbol;
  const tradeType = req.query.type;
  const startDate = moment(req.query.start, "YYYY-MM-DD");
  const endDate = moment(req.query.end, "YYYY-MM-DD");
  const trades = tradesController
    .getTrades()
    .filter((trade) => stockSymbol === trade.symbol);
  const response = {
    code: 404,
    body: {},
  };

  if (trades.length) {
    response.code = 200;
    response.body = trades
      .filter((trade) => {
        return (
          tradeType === trade.type &&
          startDate.isSameOrBefore(trade.timestamp, "day") &&
          endDate.isSameOrAfter(trade.timestamp, "day")
        );
      })
      .sort((a, b) => a.id - b.id);
  }

  res.statusCode = response.code;
  res.send(response.body);
});

// Get highest and lowest stock trades ?start={startDate}&end={endDate}
// Stock symbol does not exist 404
// 200 otherwise
router.get("/:stockSymbol/price", (req, res, next) => {
  const stockSymbol = req.params.stockSymbol;
  const startDate = moment(req.query.start, "YYYY-MM-DD");
  const endDate = moment(req.query.end, "YYYY-MM-DD");
  const trades = tradesController
    .getTrades()
    .filter((trade) => stockSymbol === trade.symbol);
  const response = {
    code: 404,
    body: {},
  };

  if (trades.length) {
    response.code = 200;

    const tradeValues = trades
      .filter((trade) => {
        return (
          startDate.isSameOrBefore(trade.timestamp, "day") &&
          endDate.isSameOrAfter(trade.timestamp, "day")
        );
      })
      .map((trade) => trade.price)
      .sort((a, b) => a - b);

    response.body = tradeValues.length
      ? {
          symbol: stockSymbol,
          highest: tradeValues[tradeValues.length - 1],
          lowest: tradeValues[0],
        }
      : {
          message: "There are no trades in the given date range",
        };
  }

  res.statusCode = response.code;
  res.send(response.body);
});

module.exports = router;
