const express = require("express");
const router = express.Router();
const tradesController = require("../controllers/trades");

// Add new trades POST
// 400 if trade exists, 201 if created
router.post("/", (req, res, next) => {
  const trade = req.body,
    response = {
      code: 201,
    };

  if (
    // !tradesController.isValidTrade(trade) ||
    tradesController.hasTrade(trade.id)
  ) {
    response.code = 400;
  } else {
    tradesController.addTrade(trade);
  }

  res.statusCode = response.code;
  res.send({});
});

// List of trades GET sorted by id ASC
router.get("/", (req, res, next) => {
  res.send(tradesController.getTrades().sort((a, b) => a.id - b.id));
});

// Trades sorted by id ASC made by a user
// 404 if user not found
// 200 otherwise
router.get("/users/:userId", (req, res, next) => {
  const userId = parseInt(req.params.userId);
  const trades = tradesController.getTrades();
  const response = {
    code: 404,
    body: {},
  };

  const userTrades = trades.filter((trade) => userId === trade.user.id);
  if (userTrades.length) {
      response.code = 200;
      response.body = userTrades.sort((a, b) => a.id - b.id);
  }

  res.statusCode = response.code;
  res.send(response.body);
});

module.exports = router;
