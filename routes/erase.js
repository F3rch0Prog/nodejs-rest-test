const express = require("express");
const router = express.Router();
const tradesController = require("../controllers/trades");

// DELETE should clear all trades and return 200
router.delete("/", (req, res, next) => {
  tradesController.clearTrades();
  res.send({});
});

module.exports = router;
